import 'package:dio_example/models/post.dart';
import 'package:flutter/material.dart';
import 'package:dio_example/components/post/post_view.dart';
import 'package:dio_example/notifiers/posts_notifier.dart';
import 'package:dio_example/services/api_service.dart';
import 'package:flutter_dialogs/flutter_dialogs.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomeScreen> {
  final _formKey = GlobalKey<FormState>();
  Post _post = new Post();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController postController = TextEditingController();
  TextEditingController postBodyController = TextEditingController();



  _createPost(BuildContext context) {
    if (!_formKey.currentState!.validate()) {
      showPlatformDialog(
        context: context,
        builder: (context) => BasicDialogAlert(
          title: Text("Form Error"),
          content:
          Text('Please enter some text for the post!!'),
          actions: <Widget>[
            BasicDialogAction(
              title: Text("Ok"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),

          ],
        ),
      );
      return;
    }
    _formKey.currentState!.save();
    _post.userId = 1;

    PostsNotifier postNotifier = Provider.of(context, listen: false);
    _post.id = postNotifier.getPostList().length + 1;
    postNotifier.uploadPost(_post).then((value) {
      if (value) {
        showPlatformDialog(
          context: context,
          builder: (context) => BasicDialogAlert(
            title: Text("Successfull!!"),
            content:
            Text('Post added succesfully!!'),
            actions: <Widget>[
              BasicDialogAction(
                title: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),

            ],
          ),
        );
      }
    });
  }



  @override
  void initState() {
    PostsNotifier postNotifier =
        Provider.of<PostsNotifier>(context, listen: false);
    ApiService.getPosts(postNotifier);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    PostsNotifier postNotifier = Provider.of<PostsNotifier>(context);

    return Scaffold(

        body: SafeArea(
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Padding(
                      padding:EdgeInsets.symmetric(vertical: 20,horizontal: 20),
                      child: Text("Posts.",style: TextStyle(fontSize: 40),),
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: postNotifier.getPostList().isNotEmpty
                      ? buildPostlist(postNotifier)
                      :  Center(child: CircularProgressIndicator()),
                )

              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _showBottomSheet(context);
          },
          child: Icon(Icons.add),
        ),

    );
  }

  _showBottomSheet(BuildContext context) {
    showModalBottomSheet(
      isScrollControlled:true,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: Container(
            color: Color.fromRGBO(0, 0, 0, 0.001),
            child: GestureDetector(
              onTap: () {},
              child: DraggableScrollableSheet(
                initialChildSize: 0.4,
                minChildSize: 0.2,
                maxChildSize: 0.75,
                builder: (_, controller) {
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(25.0),
                        topRight: const Radius.circular(25.0),
                      ),
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 20.0, left: 20.0),
                              child: Text("Add Posts",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black)),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20.0, right: 20.0),
                              child: IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Colors.grey[600],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                child: Text(
                                  "Enter your post title and post body to be added to post list.",
                                ),
                              ),
                            )
                          ],
                        ),

                        SizedBox(
                          height: 25,
                        ),

                        Expanded(
                          child: SingleChildScrollView(
                            child: Form(
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              key: _formKey,
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 25),
                                    child: TextFormField(
                                      controller: postController,
                                      decoration: InputDecoration(
                                          suffixIcon: IconButton(
                                            icon: Icon(Icons.email), onPressed: () {  },
                                          ),
                                          border: OutlineInputBorder(

                                              borderRadius: BorderRadius.circular(25)
                                          ),
                                          labelText: "Post Title"

                                      ),
                                      validator: MultiValidator(
                                          [
                                            RequiredValidator(errorText: "Post title is required"),
                                          ]
                                      ),
                                      onSaved: (String? value){
                                        _post.title=postController.text;
                                      },
                                      // keyboardType: TextInputType.number


                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 25),
                                    child: TextFormField(
                                      controller: postBodyController,
                                      decoration: InputDecoration(
                                          suffixIcon: IconButton(
                                            icon: Icon(Icons.email), onPressed: () {  },
                                          ),
                                          border: OutlineInputBorder(

                                              borderRadius: BorderRadius.circular(25)
                                          ),
                                          labelText: "Post Body"

                                      ),
                                      validator: MultiValidator(
                                          [
                                            RequiredValidator(errorText: "Post Body is required"),
                                          ]
                                      ),
                                      onSaved: (String? value){
                                        _post.body=postBodyController.text;
                                      },
                                      // keyboardType: TextInputType.number


                                    ),
                                  ),

                                  SizedBox(height: 30,),

                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [


                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(30),
                                        child: Container(
                                          height: 40,
                                          color: Color(0xFF452ad2),
                                          child:  ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              primary: Color(0xFF452ad2),
                                            ),
                                            onPressed: () {
                                              _createPost(context);
                                            },
                                            child: Row(
                                              children: <Widget>[
                                                Text("Upload Post",style: TextStyle(fontSize: 12),)


                                              ],
                                            ),

                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 30,),
                                ],
                              ),
                            ),
                          ),
                        )

                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }



}

Widget buildPostlist(PostsNotifier postNotifier) {

  return ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: postNotifier.getPostList().length,
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        return PostView(
          post: postNotifier.getPostList()[index],
          index:index
        );
      });
}


