import 'package:flutter/material.dart';
import 'package:dio_example/components/post/post_view_model.dart';
import 'package:dio_example/models/post.dart';
import 'package:dio_example/constants/constants.dart';

class PostView extends StatefulWidget {
  final Post? post;
  final int index;

  PostView({@required this.post,required this.index});

  @override
  State createState() {
    return PostViewState(post!,index);
  }
}

class PostViewState extends State<PostView> {
  Post? post;
  PostViewModel? postViewModel;
  int index;

  PostViewState(this.post, this. index){
    postViewModel = new PostViewModel();
    postViewModel!.setPost(post!);

  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(),
            borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
          child: Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                      height: 80,
                      width: 50,
                      decoration: BoxDecoration(
                          color: colours[index%4].withOpacity(0.3),
                          border: Border.all(),
                          borderRadius: BorderRadius.all(Radius.circular(20))
                      ),
                      child: Container(
                        child: Text("UID: ${postViewModel!.post!.userId}"),
                      )
                  )),
              Expanded(
                flex: 3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Expanded(flex:4,child: Text("Id: ${postViewModel!.post!.id}",style: TextStyle(fontWeight: FontWeight.w700),)),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Expanded(flex:4,child: Text("Title: ",style: TextStyle(fontWeight: FontWeight.w700),)),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Expanded(flex:4,child: Text("${postViewModel!.post!.title}"),),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Expanded(flex:4,child: Text("Body: ",style: TextStyle(fontWeight: FontWeight.w700),)),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Expanded(flex:4,child: Text("${postViewModel!.post!.body}"),),
                        ],
                      ),
                    ),
                  ],
                ),
              )



            ],
          ),
        ),
      ),
    );
  }
}
