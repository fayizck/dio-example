import 'package:flutter/material.dart';

List<Color> colours =[
  Color(0xffACDDDE),
  Color(0xffCAF1DE),
  Color(0xffF7D8BA),
  Color(0xffFFE7C7),
];