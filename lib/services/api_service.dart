import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dio_example/models/post.dart';
import 'package:dio_example/notifiers/posts_notifier.dart';
import 'package:hive/hive.dart';


import '../main.dart';


class ApiService {

  static Dio dio = Dio();
  static const String baseUrl =
      "https://jsonplaceholder.typicode.com/posts";



  static getPosts(PostsNotifier postNotifier) async {



    try{
      List<Post> postList = [];
      List posts;
      Response response = await dio.get(baseUrl);
      posts = response.data;

      var box=Hive.box(API_BOX);
      box.put("posts", posts);
      var dbData=box.get("posts");
      dbData.forEach((element) {
        postList.add(Post.fromMap(element));
      });

      print(postList.length);
      postNotifier.setPostList(postList);
    }on DioError catch (e) {
      List<Post> postList = [];
      List posts;
      var box=Hive.box(API_BOX);
      var dbData=box.get("posts");
      dbData.forEach((element) {
        postList.add(Post.fromMap(element));
      });

      print(postList.length);
      postNotifier.setPostList(postList);


    }



  }


  static Future<bool> addPost(Post post,PostsNotifier postNotifier) async{
    print("addPost");
    bool result = false;
    try {
      Response response = await dio.post(baseUrl, data: json.encode(post.toMap()),);
      print('Response status: ${response.statusCode}');
      print('User created: ${response.data}');

      if(response.statusCode == 201){
        print("successful");
        result = true;
        postNotifier.addPostToList(post);
      }
    } catch (e) {
      print('Error creating user: $e');
    }

    return result;
  }





}
